---
excerpt: 'Tinqer started off as a business idea to be a consulting company which specializes in Chat bots and AI-driven software for the healthcare industry. The main upcoming feature was  to allow any user to upload their bot to our collection and get paid for the use.'
coverImage: 'https://firebasestorage.googleapis.com/v0/b/shagunresume.appspot.com/o/project-previews%2FScreen%20Shot%202021-06-08%20at%2011.56.28%20AM.png?alt=media&token=210bb81a-b071-4b45-83f9-b319a73fac18'
showCaseLink: 'https://tinqertinquer.web.app/'
title: 'Tinqer'
---
