---
excerpt: 'A full-featured online Notepad for free, made using React and Quilljs.'
coverImage: 'https://firebasestorage.googleapis.com/v0/b/shagunresume.appspot.com/o/project-previews%2Fshagun-mistry-portfolio-menotepad.png?alt=media&token=266f6086-7a02-46e3-b5fe-94e6567cef11'
showCaseLink: 'https://menotepad.com/'
title: 'Menotepad'
---
