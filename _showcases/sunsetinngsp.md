---
excerpt: 'This site was made for a local motel so that customers can easily find its rates, contact info, and address. '
coverImage: 'https://firebasestorage.googleapis.com/v0/b/shagunresume.appspot.com/o/project-previews%2FScreen%20Shot%202021-06-08%20at%2011.51.22%20AM.png?alt=media&token=3175e053-02fb-4a21-99a3-a7c1dac46bdf'
showCaseLink: 'https://sunsetinngsp.com/'
title: 'Sunset Inn Motel'
---
