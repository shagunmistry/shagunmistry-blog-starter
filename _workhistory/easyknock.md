---
title: 'EasyKnock'
description: ''
coverImage: 'https://images.ctfassets.net/dqor5gp7cxux/3ECBZR5epWDaH8sVVgX6CM/cd3c42bad26da9bec71a46a71282b7f7/easyknock-logo-large.webp'
currentJob: 'false'
jobTitle: 'Mid-level Software Developer'
dates: '2019-2020'
---

- [x] Implemented new features on NodeJS APIs as proposed by the Product team.

---

- [x] Suggested good coding practices and standards for better code maintenance and scalability.

---

- [x] Worked with Business and Product teams to review and integrate 3r​ d​ party services such as LQ with company’s APIs.

---

- [x] Worked with Kubernetes and Kibana to improve application logging and monitoring.

---

- [x] Created AWS Lambdas and API Microservices to implement new features as required.
