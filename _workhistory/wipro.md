---
title: 'Wipro'
description: ''
coverImage: 'https://www.wipro.com/content/dam/nexus/en/brand/images/wipro-primary-logo-color-rbg.png'
currentJob: 'false'
jobTitle: 'Software Engineer'
dates: '2018-2018'
---

- [x] Met important deadlines by monitoring project’s progress and coordinating with other teams.

---

- [x] Successfully managed all stores’ DB queries and processes for my team’s applications.

---

- [x] Developed documentation, layouts, and flowcharts to identify requirements and expected solutions.

---

- [x] Troubleshooted and debugged existing applications written in Java, Shell, and Perl.

---

- [x] Managed Linux, Unix, and JBOSS applications

---

- [x] Created a python web bot that accepts remedy tickets for all teams in a certain range of time.
