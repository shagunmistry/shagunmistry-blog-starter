---
title: 'Ria Money Transfer'
description: ''
coverImage: 'https://upload.wikimedia.org/wikipedia/commons/1/1e/Ria_Main_Logo_Descriptor_Color_RGB.png'
currentJob: 'false'
jobTitle: 'Full-stack Software Developer'
dates: '2018-2019'
---

- [x] Successfully helped in developing new features in a RESTful Client-agnostic Finance API with the use of C#/.Net Core.

---

- [x] Effectively worked with multiple teams to help plan new features and improve current ones.

---

- [x] Helped the launch of their company website with the new techstack of ReactJS and C#.

---

- [x] Wrote efficient and detailed unit tests for both Front-end Reactjs codebase and C# API.
