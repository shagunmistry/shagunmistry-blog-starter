---
title: 'SourceLink'
description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Praesent elementum facilisis leo vel fringilla est ullamcorper eget. At imperdiet dui accumsan sit amet nulla facilities morbi tempus.'
coverImage: 'https://firebasestorage.googleapis.com/v0/b/shagunresume.appspot.com/o/sourcelink_logo_tag_bg.png?alt=media&token=b712143f-5f5d-4122-8c17-ed37e6066af9'
currentJob: 'false'
jobTitle: 'Enterprise Software Developer'
dates: '2017-2018'
---

- [x] Developed C# software to automate daily Jobs using Visual Studio.

---

- [x] Managed Mail Data Processing from different Clients

---

- [x] Used FoxPro to perform Data Processing and Data Management and manipulation.

---

- [x] Designed of multi-layer forms using designs and layouts in VB

---

- [x] Led the development of a full data processing automation software for a top client.
