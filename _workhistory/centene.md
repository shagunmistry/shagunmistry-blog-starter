---
title: 'Centene Corporation'
description: 'As a senior developer, my responsibilities include quality coding, mentoring, delegating, helping out leaders and junior developers.'
coverImage: 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Centene_Corporation_Logo.svg/1200px-Centene_Corporation_Logo.svg.png'
currentJob: 'true'
jobTitle: 'Senior Software Application Engineer'
dates: '2020-Present'
---

- [x] Implemented features in GOLANG APIs by collaborating for features with product teams.

---

- [x] Mentored interns and Junior developers with Angular development processes.

---

- [x] Lead the developmemt of a Progressive Web App from the ground-up.

---

- [x] Met tight deadlines by implementing features efficiently and guided the process.
