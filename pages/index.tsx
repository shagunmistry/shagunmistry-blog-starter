import { getAllShowcases, getAllWorkHistory } from '../lib/api';
import axios from 'axios';
import Container from '../components/container';
import Head from 'next/head';
import Intro from '../components/intro';
import Layout from '../components/layout';
import showcase from '../types/showcase';
import Showcases from '../components/showcases';
import WorkHistory from '../components/WorkHistory';

type Props = {
  workHistory: any;
  showCases: showcase[];
  quote: { author: string; text: string };
};

const Index = ({ workHistory, showCases, quote }: Props) => {
  return (
    <>
      <Layout showAlert={true} alertData={quote}>
        <Head>
          <title>Shagun Mistry Portfolio</title>
          <link
            href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
            crossOrigin="anonymous"
          />
        </Head>
        <Container>
          <Intro />
          <WorkHistory workHistory={workHistory} />
          <br />
          {showCases.length > 0 && <Showcases showcases={showCases} />}
          <br />
          {/* {allPosts.length > 0 && <MoreStories posts={allPosts} />} */}
        </Container>
      </Layout>
    </>
  );
};

export default Index;

export const getStaticProps = async () => {
  const allWorkHistory = getAllWorkHistory();
  const allShowcases = getAllShowcases();

  let quote = null;
  try {
    const quoteRes = await axios.get('https://zenquotes.io/api/random');
    if (quoteRes && quoteRes.status >= 200 && quoteRes.status < 400) {
      quote = {
        author: quoteRes.data[0]?.a,
        text: quoteRes.data[0]?.q
      };
    }
  } catch (e: any) {
    // do nothing.
  }

  return {
    props: { workHistory: allWorkHistory, showCases: allShowcases, quote }
  };
};
