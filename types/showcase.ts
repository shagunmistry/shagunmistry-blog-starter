export type showcase = { coverImage: string; excerpt: string; showCaseLink: string; title: string };
export default showcase;
