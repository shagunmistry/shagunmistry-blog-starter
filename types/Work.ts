export type Work = {
  content: string;
  coverImage: string;
  currentJob: string;
  dates: string;
  description: string;
  jobTitle: string;
  title: string;
};
