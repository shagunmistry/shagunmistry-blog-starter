import Container from './container';

type Props = {
  alertData?: { author: string; text: string };
};

const Alert = ({ alertData }: Props) => {
  return (
    <div className="text-white box-shadow-1">
      {alertData ? (
        <Container>
          <div className="py-2 text-center text-md">
            <i>{alertData.text}</i>
          </div>
          <div className="py-1 text-center text-sm">
            <strong>{alertData.author}</strong>
          </div>
        </Container>
      ) : (
        <Container>
          <div className="py-2 text-center text-md">Hi there!</div>
        </Container>
      )}
    </div>
  );
};

export default Alert;
