import Alert from './alert';
import Footer from './footer';
import Meta from './meta';

type Props = {
  showAlert?: boolean;
  children?: React.ReactNode;
  alertData?: { author: string; text: string };
};

const Layout = ({ showAlert, children, alertData }: Props) => {
  return (
    <>
      <Meta />
      <div className="min-h-screen">
        {showAlert ? <Alert alertData={alertData} /> : <></>}
        <main>{children}</main>
      </div>
      <Footer />
    </>
  );
};

export default Layout;
