import { faAngular, faAws, faCss3, faDocker, faGoogle, faMicrosoft, faPython, faReact } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Avatar from './avatar';
import ContactGroup from './contact-group';

const Intro = () => {
  return (
    <>
      <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
        <Avatar
          name={'Shagun Mistry'}
          picture={
            'https://firebasestorage.googleapis.com/v0/b/shagunresume.appspot.com/o/Shagun_mistry_web_headshot.jpg?alt=media&token=d8508382-1230-4f41-8002-da2c26b8046d'
          }
        />
        <h3 className="text-center text-2xl md:text-3xl font-bold tracking-tighter leading-tight md:pr-8">
          A developer & an entrepreneur working to improve one issue at a time.
        </h3>
      </section>
      <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
        <h1 className="text-3xl md:text-3xl  tracking-tighter leading-tight md:pr-8">Contact Me:</h1>
        <div className="tracking-tighter leading-tight md:pr-8">
          <ContactGroup />
        </div>
      </section>
      <hr />
      <br />
      <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
        <h1 className="text-3xl md:text-3xl  tracking-tighter leading-tight md:pr-8">Tech used:</h1>
        <div className="tracking-tighter leading-tight md:pr-8">
          <div className="btn-group icons-intro-group" role="group">
            <a type="button" className="contact-list-button" style={{ color: '#B52E31' }}>
              <FontAwesomeIcon icon={faAngular} className="icons-intro-icon" />
            </a>
            <a type="button" className="contact-list-button" style={{ color: '#264de4' }}>
              <FontAwesomeIcon icon={faCss3} className="icons-intro-icon" />
            </a>
            <a type="button" className="contact-list-button" style={{ color: '#264de4' }}>
              <FontAwesomeIcon icon={faReact} className="icons-intro-icon" />
            </a>
            <a type="button" className="contact-list-button" style={{ color: '#00A4EF' }}>
              <FontAwesomeIcon icon={faMicrosoft} className="icons-intro-icon" />
            </a>
            <a type="button" className="contact-list-button" style={{ color: '#0db7ed' }}>
              <FontAwesomeIcon icon={faPython} className="icons-intro-icon" />
            </a>
            <a type="button" className="contact-list-button" style={{ color: '#0db7ed' }}>
              <FontAwesomeIcon icon={faDocker} className="icons-intro-icon" />
            </a>
            <a type="button" className="contact-list-button" style={{ color: '#FF9900' }}>
              <FontAwesomeIcon icon={faAws} className="icons-intro-icon" />
            </a>
            <a type="button" className="contact-list-button" style={{ color: '#5a87c5' }}>
              <FontAwesomeIcon icon={faGoogle} className="icons-intro-icon" />
            </a>
          </div>
        </div>
      </section>
    </>
  );
};

export default Intro;
