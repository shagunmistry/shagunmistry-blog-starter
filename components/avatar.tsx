type Props = {
  name: string;
  picture: string;
};

const Avatar = ({ name, picture }: Props) => {
  return (
    <div className="flex items-center">
      <img src={picture} className="rounded-full mr-4 avatar-img" alt={name} />
      <div className="text-3xl font-bold">{name}</div>
    </div>
  );
};

export default Avatar;
