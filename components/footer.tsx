import Container from './container';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAt, faPhone } from '@fortawesome/free-solid-svg-icons';
import ContactGroup from './contact-group';

const Footer = () => {
  return (
    <footer className="body-footer">
      <Container>
        <div className="py-28 flex flex-col lg:flex-row items-center">
          <h3 className="text-4xl lg:text-5xl font-bold tracking-tighter leading-tight text-center lg:text-left mb-10 lg:mb-0 lg:pr-4 lg:w-1/2">
            Coding is the future.
          </h3>
          <ContactGroup />
        </div>
      </Container>
    </footer>
  );
};

export default Footer;
