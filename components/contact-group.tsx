import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAt, faDownload, faPhoneSquare } from '@fortawesome/free-solid-svg-icons';
import { faGithub, faLinkedin } from '@fortawesome/free-brands-svg-icons';

const ContactGroup = () => {
  return (
    <div className="btn-group icons-intro-group" role="group">
      <a
        type="button"
        className="contact-list-button"
        href="https://github.com/shagunmistry"
        data-bs-toggle="tooltip"
        data-bs-placement="top"
        title="https://github.com/shagunmistry"
      >
        <FontAwesomeIcon icon={faGithub} className="icons-intro-icon box-shadow-1" />
      </a>
      <a
        type="button"
        className="contact-list-button"
        style={{ color: '#2867b2' }}
        href="https://www.linkedin.com/in/shagun-mistry"
        data-bs-toggle="tooltip"
        data-bs-placement="top"
        title="Go to my linkedin: https://www.linkedin.com/in/shagun-mistry"
      >
        <FontAwesomeIcon icon={faLinkedin} className="icons-intro-icon box-shadow-1" />
      </a>
      <a
        type="button"
        className="contact-list-button"
        style={{ color: '#30b7ba' }}
        href="mailto:shagun.mistry@hotmail.com"
        data-bs-toggle="tooltip"
        data-bs-placement="top"
        title="Email me at shagun.mistry@hotmail.com"
      >
        <FontAwesomeIcon icon={faAt} className="icons-intro-icon box-shadow-1" />
      </a>
      <a
        type="button"
        className="contact-list-button"
        style={{ color: '#ffed00' }}
        href="tel:+18642835100"
        data-bs-toggle="tooltip"
        data-bs-placement="top"
        title="Call me at +18642835100"
      >
        <FontAwesomeIcon icon={faPhoneSquare} className="icons-intro-icon box-shadow-1" />
      </a>
      <a
        type="button"
        className="contact-list-button"
        style={{ color: '#5a87c5' }}
        href="https://firebasestorage.googleapis.com/v0/b/shagunresume.appspot.com/o/shagun_mistry_resume_post(2).docx?alt=media&token=baa259c1-c07a-4d0a-bd05-5e8a1b8c09f3"
        data-bs-toggle="tooltip"
        data-bs-placement="top"
        title="Download my Resume"
      >
        <FontAwesomeIcon icon={faDownload} className="icons-intro-icon box-shadow-1" />
      </a>
    </div>
  );
};

export default ContactGroup;
