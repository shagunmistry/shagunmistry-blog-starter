import showcase from '../types/showcase';
import ShowcasePreview from './showcase-preview';

type Props = {
  showcases: showcase[];
};

const Showcases = ({ showcases }: Props) => {
  return (
    <section>
      <h2 className="mb-8 text-6xl md:text-7xl font-bold tracking-tighter leading-tight">Projects</h2>
      <hr style={{ height: '10px' }} />
      <br />
      <div className="grid grid-cols-1 md:grid-cols-3 md:gap-x-16 lg:gap-x-16 gap-y-20 md:gap-y-32 mb-15">
        {showcases.map((showcase) => (
          <ShowcasePreview
            coverImage={showcase.coverImage}
            excerpt={showcase.excerpt}
            key={showcase.title.trim()}
            showCaseLink={showcase.showCaseLink}
            title={showcase.title}
          />
        ))}
      </div>
    </section>
  );
};

export default Showcases;
