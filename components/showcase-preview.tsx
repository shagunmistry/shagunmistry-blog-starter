import CoverImage from './cover-image';
import Link from 'next/link';

type Props = {
  coverImage: string;
  excerpt: string;
  showCaseLink: string;
  title: string;
};

const ShowcasePreview = ({ coverImage, excerpt, showCaseLink, title }: Props) => {
  return (
    <div className="card showcase-card">
      <div className="card-body">
        <div className="showcase-cover-image">
          <CoverImage title={title} src={coverImage} />
        </div>
        <h3 className="text-3xl mb-3 leading-snug">
          <Link as={showCaseLink} href={showCaseLink}>
            <a className="hover:underline">{title}</a>
          </Link>
        </h3>
        <p className="text-lg leading-relaxed mb-4">{excerpt}</p>
      </div>
    </div>
  );
};

export default ShowcasePreview;
