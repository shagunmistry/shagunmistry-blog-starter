import ReactMarkdown from 'react-markdown';
import gfm from 'remark-gfm';

type Props = {
  content: string;
};

const WorkHistoryBody = ({ content }: Props) => {
  return (
    <div className="max-w-2xl mx-auto">
      <ReactMarkdown remarkPlugins={[gfm]} children={content} />
    </div>
  );
};

export default WorkHistoryBody;
