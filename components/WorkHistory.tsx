import { Work } from '../types/Work';
import markdownToHtml from '../lib/markdownToHtml';
import WorkHistoryBody from './workhistory-body';

type Props = {
  workHistory: Work[];
};

const WorkHistory = ({ workHistory }: Props) => {
  const getHTML = async (content: string) => {
    const val = await markdownToHtml(content);
    return val;
  };

  return (
    <section className="section-container">
      <section className="flex-col md:flex-row flex items-center md:justify-between mt-16 mb-16 md:mb-12">
        <h1 className="text-5xl md:text-6xl font-bold tracking-tighter leading-tight md:pr-8">Work History</h1>
      </section>
      <h4 className="text-center md:text-left text-lg mt-5 md:pl-8">Teamwork Makes the Dream Work.</h4>
      <hr style={{ height: '10px', marginBottom: '15px' }} />
      <div className="row row-cols-lg-3 row-cols-md-2 row-cols-1">
        {workHistory && workHistory.length > 0
          ? workHistory.map((each) => {
              return (
                <div className="col" key={each.title}>
                  <div className={`card work-card box-shadow-1 ${each.currentJob === 'true' ? 'current-job-card' : ''}`}>
                    <img src={each.coverImage} className="card-img-top" alt={each.title} />
                    <div className="card-body">
                      <h3 className="text-center text-2xl md:text-3xl font-bold tracking-tighter leading-tight md:pr-8">{each.jobTitle}</h3>
                      <br />
                      <WorkHistoryBody content={each.content} />
                    </div>
                    <div className="card-footer work-card-footer">
                      <small>{each.dates}</small>
                    </div>
                  </div>
                </div>
              );
            })
          : null}
      </div>
    </section>
  );
};

// export async function getStaticProps() {
//   const content = await markdownToHtml(post.content || '');

//   return {
//     props: {
//       post: {
//         ...post,
//         content
//       }
//     }
//   };
// }

export default WorkHistory;
