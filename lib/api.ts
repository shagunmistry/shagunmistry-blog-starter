import fs from 'fs';
import { join } from 'path';
import matter from 'gray-matter';
import { Work } from '../types/Work';

const postsDirectory = join(process.cwd(), '_posts');
const workHistoryDirectory = join(process.cwd(), '_workhistory');
const showCasesDirectory = join(process.cwd(), '_showcases');

export function getPostSlugs() {
  return fs.readdirSync(postsDirectory);
}

export function getWorkHistorySlugs() {
  return fs.readdirSync(workHistoryDirectory);
}

export function getshowCaseSlugs() {
  return fs.readdirSync(showCasesDirectory);
}

/** Blog Posts */

export function getPostBySlug(slug: string, fields: string[] = []) {
  const realSlug = slug.replace(/\.md$/, '');
  const fullPath = join(postsDirectory, `${realSlug}.md`);
  const fileContents = fs.readFileSync(fullPath, 'utf8');
  const { data, content } = matter(fileContents);

  type Items = {
    [key: string]: string;
  };

  const items: Items = {};

  // Ensure only the minimal needed data is exposed
  fields.forEach((field) => {
    if (field === 'slug') {
      items[field] = realSlug;
    }
    if (field === 'content') {
      items[field] = content;
    }

    if (data[field]) {
      items[field] = data[field];
    }
  });

  return items;
}

export function getAllPosts(fields: string[] = []) {
  const slugs = getPostSlugs();
  const posts = slugs
    .map((slug) => getPostBySlug(slug, fields))
    // sort posts by date in descending order
    .sort((post1, post2) => (post1.date > post2.date ? -1 : 1));
  return posts;
}

/** Work History */

export function getWorkHistoryBySlugs(slug: string) {
  const realSlug = slug.replace(/\.md$/, '');
  const fullPath = join(workHistoryDirectory, `${realSlug}.md`);
  const fileContents = fs.readFileSync(fullPath, 'utf8');
  const { data, content } = matter(fileContents);

  type Items = {
    [key: string]: string;
  };

  const items: Items = {};
  const fields = ['content', 'coverImage', 'currentJob', 'dates', 'description', 'jobTitle', 'title'];

  // Ensure only the minimal needed data is exposed
  fields.forEach((field) => {
    if (field === 'slug') {
      items[field] = realSlug;
    }
    if (field === 'content') {
      items[field] = content;
    }

    if (data[field]) {
      items[field] = data[field];
    }
  });

  return items;
}

export function getAllWorkHistory() {
  const workHistorySlugs = getWorkHistorySlugs();
  const workHistory = workHistorySlugs.map((slug) => {
    return getWorkHistoryBySlugs(slug);
  });
  return workHistory;
}

/** Showcases */
export function getShowcaseBySlug(slug: string) {
  const realSlug = slug.replace(/\.md$/, '');
  const fullPath = join(showCasesDirectory, `${realSlug}.md`);
  const fileContents = fs.readFileSync(fullPath, 'utf8');
  const { data, content } = matter(fileContents);

  type Items = {
    [key: string]: string;
  };

  const items: Items = {};
  const fields = ['excerpt', 'coverImage', 'showCaseLink', 'title'];

  // Ensure only the minimal needed data is exposed
  fields.forEach((field) => {
    if (field === 'slug') {
      items[field] = realSlug;
    }

    if (data[field]) {
      items[field] = data[field];
    }
  });

  return items;
}

export function getAllShowcases() {
  const showCaseSlugs = getshowCaseSlugs();
  const workHistory = showCaseSlugs.map((slug) => {
    return getShowcaseBySlug(slug);
  });
  return workHistory;
}
